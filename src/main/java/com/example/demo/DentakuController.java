package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DentakuController{

	@RequestMapping(value = "/index")
	public String index(){
		return "index";
	}
	
	@RequestMapping(value = "/edit")
	public String edit(){
		return "edit";
	}
	
	@RequestMapping(value = "/drop")
	public String drop(){
		return "drop";
	}
	
	@RequestMapping(value = "/learn")
	public String learn(){
		return "learn";
	}
	
	@RequestMapping(value = "/graph")
	public String graph(){
		return "graph";
	}
}
